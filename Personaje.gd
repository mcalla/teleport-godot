extends CharacterBody3D

const SPEED = 8.0
const JUMP_VELOCITY = 4.5

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

# Root
@onready var root = get_tree().root

# Camara
@onready var camara = get_node("Camera3D")
const anguloMin = -90 
const anguloMax = 90
const sensCamara : float = .18
var mouseDelta : Vector2 = Vector2()
const rayLength = 10000

# Crosshair
@onready var crosshair = get_node("../crosshair")

# SubViewport
@onready var subViewport = get_node("../PantallaTeleport")
@onready var camSubViewport = get_node("../PantallaTeleport/SubViewportContainer/SubViewport/Camera3D")
@onready var boxDebug = get_node("../PantallaTeleport/SubViewportContainer/SubViewport/Camera3D/BoxDebug")
func _input(event):
	if event is InputEventMouseMotion:
		mouseDelta = event.relative

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	subViewport.hide()

func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit()
	
	if Input.is_action_pressed("RMB"):
		castingTeleport()
	
	if Input.is_action_just_released("RMB"):
		subViewport.hide()
		crosshair.crosshairColor = 0xffffffff
		crosshair.updateCrosshair()
		
	#Limitacion del movimiento de la camara en el eje X
	camara.rotation_degrees.x = clamp(camara.rotation_degrees.x, anguloMin, anguloMax)
			
	#Movimiento de la camara en el eje X
	camara.rotation_degrees -= Vector3(rad_to_deg(mouseDelta.y), 0, 0) * delta * sensCamara
			
	#Movimiento de la camara en el eje Y
	rotation_degrees -= Vector3(0, rad_to_deg(mouseDelta.x), 0) * delta * sensCamara
			
	#Reinicio el mouseDelta
	mouseDelta = Vector2()
	

func _physics_process(delta):
	# Add the gravity.
	if not is_on_floor():
		velocity.y -= gravity * delta

	# Handle jump.
	if Input.is_action_just_pressed("ui_accept") and is_on_floor():
		velocity.y = JUMP_VELOCITY

	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var input_dir = Input.get_vector("ui_left", "ui_right", "ui_up", "ui_down")
	var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	if direction:
		velocity.x = direction.x * SPEED
		velocity.z = direction.z * SPEED
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
		velocity.z = move_toward(velocity.z, 0, SPEED)

	move_and_slide()

func castingTeleport():
	subViewport.show()
	var space_state = get_world_3d().direct_space_state 
	var mousePos = get_viewport().get_mouse_position()
	mousePos.y = get_viewport().size.y / 2
	var origin = camara.project_ray_origin(mousePos)
	var end = origin + camara.project_ray_normal(mousePos) * rayLength
	var query = PhysicsRayQueryParameters3D.create(origin, end)
	query.exclude = [self]
	
	var result = space_state.intersect_ray(query)
	if result:
		#print(result.position)
		# Muevo la camara sobre la normal del plano donde interseca el rayo
		var posCamara : Vector3 = result.normal * 5
		boxDebug.position = Vector3(result.position.x, result.position.y + 5, result.position.z)
		# Roto la camara en direccion del punto de interseccion
		camSubViewport.position = posCamara
		var at : Transform3D = camSubViewport.get_global_transform()
		var at_cam = at.looking_at(result.position)
		#print(at_cam)
		camSubViewport.set_global_transform(at_cam)
		camSubViewport.make_current()
		crosshair.crosshairColor = 0x00ff00ff
		crosshair.updateCrosshair()
		if Input.is_action_just_pressed("LMB"):
			executeTeleport(result.position)
	else:
		camSubViewport.clear_current()
		crosshair.crosshairColor = 0xff0000ff
		crosshair.updateCrosshair()

func executeTeleport(pos_ : Vector3):
	position = pos_
